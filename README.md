# Bloody Fabric!

This mod makes it so that mobs you attack emit simple blood particles! Add some visceral joy to your survival games!

My goal is to make combat impactful, visceral, and visually pleasing, without adding any assets to the game. One of my inspirations for the visuals I hope to achieve is Brutal Doom, a mod for Doom 1 and Doom 2.

This project also serves as a learning opportunity. Feel free to submit pull requests.

## License

This mod is available under the CC0 license.

## Credits

[Fabric Example Mod](https://github.com/FabricMC/fabric-example-mod)

Inspiration from [Modding Legacy's blood-particles](https://gitlab.com/modding-legacy/blood-particles)