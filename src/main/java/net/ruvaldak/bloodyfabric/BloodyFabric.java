package net.ruvaldak.bloodyfabric;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.player.AttackEntityCallback;
import net.minecraft.block.Blocks;
import net.minecraft.client.particle.SpriteProvider;
import net.minecraft.client.particle.WaterSplashParticle;
import net.minecraft.client.texture.Sprite;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.SkeletonEntity;
import net.minecraft.entity.mob.SlimeEntity;
import net.minecraft.item.*;
import net.minecraft.particle.*;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BloodyFabric implements ModInitializer {

    public static final Logger LOGGER = LoggerFactory.getLogger("bloodyfabric");

    // @formatter:off
    private static final BlockStateParticleEffect // particle types
            SLIME_PARTICLE = new BlockStateParticleEffect(ParticleTypes.BLOCK, Blocks.SLIME_BLOCK.getDefaultState()),
            DEFAULT_PARTICLE = new BlockStateParticleEffect(ParticleTypes.BLOCK, Blocks.REDSTONE_BLOCK.getDefaultState());

    private static final Vec3f // colors in 0...1 range
            BLOOD_RED = new Vec3f(0.65f, 0.0f, 0.0f),
            SLIME_GREEN = new Vec3f(0.221f, 0.288f, 0.213f);
    // @formatter:on

    // get random number in range min...max
    public float getRandomNumber(float min, float max) {
        return (float) ((Math.random() * (max - min)) + min);
    }

    @Override
    public void onInitialize() {
        LOGGER.info("Time to bleed!");

        // when player attacks entity
        AttackEntityCallback.EVENT.register((player, world, hand, entity, hitResult) -> {
            if (!(entity instanceof LivingEntity livingEntity)) return ActionResult.PASS;
            if (!(livingEntity.canTakeDamage())) { /*LOGGER.info(livingEntity.getName().getString() + " can't be hit right now!");*/ return ActionResult.PASS; }

            //LOGGER.info("you hit a " + livingEntity.getName().getString() + "!");

            float amount = 2.3f;
            int numParticles;

            BlockStateParticleEffect particle;
            Vec3f color;
            boolean mist = false;

            if(livingEntity instanceof SkeletonEntity)
                return ActionResult.PASS;
            else if(livingEntity instanceof SlimeEntity) {
                particle = SLIME_PARTICLE;
                color = SLIME_GREEN;
            }
            else {
                particle = DEFAULT_PARTICLE;
                color = BLOOD_RED;
                mist = true;
            }

            float attackDamage = 0.0f;
            Item item = player.getStackInHand(hand).getItem();
            if(item instanceof SwordItem sword) {
                //LOGGER.info("you hit with a sword!");
                attackDamage = sword.getAttackDamage();
                //LOGGER.info("your attack damage is " + attackDamage);
            }
            if(item instanceof AxeItem axe) {
                //LOGGER.info("you hit with an axe!");
                attackDamage = axe.getAttackDamage();
                //LOGGER.info("your attack damage is " + attackDamage);
            }
            if(item instanceof PickaxeItem pickaxe) {
                //LOGGER.info("you hit with a pickaxe!");
                attackDamage = pickaxe.getAttackDamage();
                //LOGGER.info("your attack damage is " + attackDamage);
            }
            if(item instanceof ShovelItem shovel) {
                //LOGGER.info("you hit with a shovel!");
                attackDamage = shovel.getAttackDamage();
                //LOGGER.info("your attack damage is " + attackDamage);
            }
            if(item instanceof HoeItem hoe) {
                //LOGGER.info("you hit with a shovel!");
                attackDamage = hoe.getAttackDamage();
                //LOGGER.info("your attack damage is " + attackDamage);
            }
            //LOGGER.info("You hit with " + item.getName().getString() + "!");

            numParticles = 5+(int)Math.pow(attackDamage, amount);

            double entityBoxLengthX = livingEntity.getBoundingBox().getXLength();
            double entityBoxLengthY = livingEntity.getBoundingBox().getYLength();
            double entityBoxLengthZ = livingEntity.getBoundingBox().getZLength();
            Vec3d entityBoxCenter = livingEntity.getBoundingBox().getCenter();

            //LOGGER.info("Spawning " + numParticles + " blood particles!");

            if(livingEntity.getHealth() > 0.0f) {
                for(int i = 0; i < numParticles; i++) {
                    float randomX = getRandomNumber((float)(entityBoxCenter.x - (entityBoxLengthX/2)), (float)(entityBoxCenter.x + (entityBoxLengthX/2)));
                    float randomY = getRandomNumber((float)(entityBoxCenter.y - (entityBoxLengthY/2)), (float)(entityBoxCenter.y + (entityBoxLengthY/2)));
                    float randomZ = getRandomNumber((float)(entityBoxCenter.z - (entityBoxLengthZ/2)), (float)(entityBoxCenter.z + (entityBoxLengthZ/2)));
                    world.addParticle(particle, false, entityBoxCenter.x, randomY, entityBoxCenter.z, 0, 0, 0);
                    if(i%2==0 && mist) {
                        world.addParticle(new DustParticleEffect(color, 0.8f), randomX, randomY, randomZ, 0.0f, 0.0f, 0.0f);
                        //world.addParticle(new WaterSplashParticle.SplashFactory(SpriteProvider), )
                    }
                }
            }



            return ActionResult.PASS;
        });
    }
}
